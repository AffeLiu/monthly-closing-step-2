SELECT
vci.purchase_id
,vci.value_card_id
,vci.id vci_id
,CONCAT('product/',LOWER(ps.type),'/',ps.product_id) ern
,vci.product_id
,vci.quantity
,pt.balanced_qty
-- ,vci.used_quantity
,(vci.quantity - pt.balanced_qty) unbalanced_quantity
,vci.name
,vci.price
,((vci.quantity - vci.used_quantity) * vci.price) unused_amount
,ps.cost
,((vci.quantity - pt.balanced_qty) * ps.cost) unused_cost
FROM share.value_card_item vci
LEFT JOIN share.product_snapshot ps ON (vci.product_snapshot_id = ps.id)
INNER JOIN (
   SELECT
    pt.purchase_id
    ,pt.item_id
    ,SUM(pt.balanced_quantity_diff) balanced_qty
    FROM share.prepay_transaction pt
--     WHERE pt.cdate BETWEEN '' AND ''  -- for historical data
    GROUP BY pt.item_id
-- HAVING SUM(pt.balanced_quantity_diff) > 0
    ) pt ON (pt.item_id = vci.id)
WHERE (vci.quantity - pt.balanced_qty) > 0
-- AND vci.cdate < ''  -- for historical data
