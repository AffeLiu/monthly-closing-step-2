SELECT
re.id
,re.purchase_id
,vci.id
,vci.name
,vci.price
,ps.cost
,rp.quantity return_quantity
,up.total up_total
,re.total refund_total
,re.ezcash_rollback
,re.ezcash_refund
,DATE_FORMAT(rp.cdate, '%Y-%m') cdate
FROM share.refund_products rp
LEFT JOIN share.refund re ON (re.id = rp.refund_id)
LEFT JOIN share.value_card_item vci ON (vci.purchase_id = re.purchase_id AND vci.product_id = rp.product_id)
LEFT JOIN share.union_purchase up ON (up.id = re.purchase_id)
LEFT JOIN share.product_snapshot ps ON (ps.id = vci.product_snapshot_id)
ORDER BY re.purchase_id

-- offsets
SELECT
up.id
,up.total
,up.send_fee
,up.bank_point_offset
,up.coupon_code_offset
,up.point_offset
,up.magic_offset
,up.channel_offset
FROM share.union_purchase up
WHERE up.id IN (

)
