SELECT
ro.id                                                           ro_id
,roi.id                                                         roi_id
,ro.purchase_id                                                 order_id_from_ro
,roi.type
,CASE roi.type
     WHEN 'VOUCHER' THEN roi.purchase_item_id
     WHEN 'VALUE_CARD_ITEM' THEN roi.value_card_item_id
     ELSE 'UNKNOWN'
END                                                             item_id
,CASE roi.type
     WHEN 'VOUCHER' THEN oi.name
     WHEN 'VALUE_CARD_ITEM' THEN vci.name
     ELSE 'UNKNOWN'
END                                                             name
,roi.returned_quantity
,CASE roi.type
     WHEN 'VOUCHER' THEN oi.price
     WHEN 'VALUE_CARD_ITEM' THEN vci.price
     ELSE 'UNKNOWN'
END                                                             price
,ps.cost
,ro.cdate
,DATE_FORMAT(ro.cdate, '%Y/%m')                                 period
FROM share.return_order_item roi
INNER JOIN share.return_order ro ON (roi.return_order_id = ro.id)
LEFT JOIN share.order_item oi ON (oi.id = roi.purchase_item_id)
LEFT JOIN share.value_card_item vci ON (vci.id = roi.value_card_item_id)
LEFT JOIN share.product_snapshot ps ON (
CASE roi.type
     WHEN 'VOUCHER' THEN oi.product_snapshot_id
     WHEN 'VALUE_CARD_ITEM' THEN vci.product_snapshot_id
     ELSE 'UNKNOWN'
END = ps.id)
WHERE ro.cdate BETWEEN '2015-01-01' AND '2016-01-01'
AND ro.is_processed = 1
ORDER BY order_id_from_ro ASC

-- SPLIT item
SELECT
vc.id
,vci.id
,vc.purchase_id
,vc.payment
FROM share.value_card_item vci
INNER JOIN share.value_card vc ON (vci.value_card_id = vc.id)
WHERE vc.payment != 'SPLIT'
AND vc.purchase_id IN (
SELECT
vc.purchase_id
FROM share.value_card vc
WHERE vc.payment= 'SPLIT'
)
AND vc.cdate > '2016-01-01'

-- offsets
SELECT
up.id
,up.total
,up.send_fee
,up.bank_point_offset
,up.coupon_code_offset
,up.point_offset
,up.magic_offset
,up.channel_offset
FROM share.union_purchase up
WHERE up.id IN (

)
