(SELECT
cc.total
,cc.used
,cc.offset
,SUM(cc.total * cc.offset)
,SUM(cc.used * cc.offset)
,'合庫'
FROM share.coupon_codes cc
WHERE cc.meta LIKE '%TCB%'
AND cc.end_date < '2015-12-01'
) UNION (
SELECT
cc.total
,cc.used
,cc.offset
,SUM(cc.total * cc.offset)
,SUM(cc.used * cc.offset)
,'AMEX'
FROM share.coupon_codes cc
WHERE cc.meta LIKE '%AMEX%'
AND cc.end_date < '2015-12-01'
) UNION (
SELECT
cc.total
,cc.used
,cc.offset
,SUM(cc.total * cc.offset)
,SUM(cc.used * cc.offset)
,'國泰美食金'
FROM share.coupon_codes cc
WHERE cc.meta LIKE '%CATHAYBK%'
AND cc.end_date < '2015-12-01'
) UNION (
SELECT
cc.total
,cc.used
,cc.offset
,SUM(cc.total * cc.offset)
,SUM(cc.used * cc.offset)
,'hami_pass'
FROM share.coupon_codes cc
WHERE cc.meta LIKE '%CHT%'
OR cc.meta LIKE '%hami_pass%'
AND cc.end_date < '2015-12-01'
) UNION (
SELECT
cc.total
,cc.used
,cc.offset
,SUM(cc.total * cc.offset)
,SUM(cc.used * cc.offset)
,'skbank'
FROM share.coupon_codes cc
WHERE cc.meta LIKE '%skbank%'
AND cc.end_date < '2015-12-01'
) UNION (
SELECT
cc.total
,cc.used
,cc.offset
,SUM(cc.total * cc.offset)
,SUM(cc.used * cc.offset)
,'上海銀行'
FROM share.coupon_codes cc
WHERE cc.meta LIKE '%Shanghai Bank%'
AND cc.end_date < '2015-12-01'
) UNION (
SELECT
cc.total
,cc.used
,cc.offset
,SUM(cc.total * cc.offset)
,SUM(cc.used * cc.offset)
,'國泰edm'
FROM share.coupon_codes cc
WHERE cc.meta LIKE '%CathayBank%'
AND cc.end_date < '2015-12-01'
)
